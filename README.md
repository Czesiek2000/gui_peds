<div align="center"><h1>GUI PEDS </h1></div>

This is simple script that allows you to change ped skin easily with build in game CEF.

</br>

## Instalation :rocket:
* Download :arrow_down: or clone this repository from [here](gitlab.com/Czesiek2000/gui_peds)

```bash
# To clone this repository use: 
git clone gitlab.com/Czesiek2000/gui_peds
```
* Copy every :page_facing_up: and :file_folder: from `client_packages` and place it in `server-files/client_packages`. If you have root `index.js` file add `require('gui_peds')` to this file. If don't have `index.js` file and add `require('gui_peds')`.

* Copy all :page_facing_up: and :file_folder: from `packages` folder and place it in `server-files/packages` folder. If you have root `index.js` file add `require('gui_peds')` to it. If not create that file and add `require('gui_peds')`.

* Open the game and press **F5** to open UI. 

## Change :key:
To change key that lauch UI open `client_packages/gui_peds/index.js` file and in the first line you will see variable that contains key that open UI. It should look similar to this: 
```js
// Change this if you want another key to open the peds gui
const key = 0x74; // 0x74 - this value should be changed.
```

Check all available [keys](https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes) you can use

## Preview from game :camera:
![preview](./preview.png)

</br>
 
If you :heart: or :thumbsup: this resource, don't forget to leave a :star: and leave :speech_baloon: on the RAGEMP [forum](https://rage.mp/forums/).
