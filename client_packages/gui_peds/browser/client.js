// Get all images and store it in variable
var images = $('.changePed');

// Listen for click event on each image
images.on('click', function () {
  console.log($(this).data('name'));
  let name = $(this).data('name');

  // Trigger event to send data from cef to client with ped name parameter
  mp.trigger('changePed', name)

  mp.trigger("visible")
})

// Close the browser when user click on cross
let close = $('.cross');
close.on('click', () => {
  $('body').hide();

  // Trigger cef to client event to disable cursor and show chat
  mp.trigger("close")
})
