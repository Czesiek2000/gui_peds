// Change this if you want another key to open the peds gui
const key = 0x74; // F5

// // Create browser
let browser = null;


mp.keys.bind(key, true, function() {
    mp.events.callRemote('keypress:F5');
    browser = mp.browsers.new('package://gui_peds/browser/index.html');
    setTimeout(() => {
      mp.gui.cursor.show(true, true);
    }, 500);
});

// Get ped from cef
mp.events.add("changePed", (ped) => {
  mp.events.callRemote('change', ped);
})

// Close CEF
mp.events.add("close", () => {
    browser.destroy();
    
    setTimeout(() => {
      mp.gui.cursor.show(false, false);
    }, 500);

})